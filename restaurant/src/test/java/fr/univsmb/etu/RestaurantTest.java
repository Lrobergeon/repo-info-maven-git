package fr.univsmb.etu;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class RestaurantTest {

	@Test
	public void shouldNotServeTicketIfItIsNotPrepared()
	{
		Commande c = new Commande();
		Ticket t = new Ticket();
		
		c.listeTickets.add(t);
		t.commande = c;
		
		t.suivant();
		Assert.assertFalse( t.etat instanceof Servi);
		
		Prepare etat = new Prepare( t );
		t.changeEtat(etat);
		
		t.suivant();
		Assert.assertTrue( t.etat instanceof Servi);
	}
	
	
	@Test
	public void shouldBeginCommandWhenFirstTicketIsSent()
	{
		Commande c = new Commande();
		Ticket t1 = new Ticket();
		Ticket t2 = new Ticket();
		
		c.listeTickets.add(t1);
		t1.commande = c;
		c.listeTickets.add(t2);
		t2.commande = c;
		
		Assert.assertFalse( c.etat == EtatCommande.EN_COUR );
		
		t1.envoie();
		
		Assert.assertTrue( c.etat == EtatCommande.EN_COUR );
		
	}
	

}
