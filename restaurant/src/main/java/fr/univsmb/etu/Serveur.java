package fr.univsmb.etu;

import java.util.ArrayList;

/**
 *
 */
public class Serveur
{

	/**
	 *
	 */
	protected String			nom;

	/**
	 *
	 */
	protected ArrayList<Table>	listeTables;

	/**
	 * Default constructor
	 */
	public Serveur ( String n ) {
		this.nom = n;
		this.listeTables = new ArrayList<Table>();
	}

	public ArrayList<Table> getListeTables ()
	{
		return this.listeTables;
	}

	public void setListeTables ( ArrayList<Table> listeTables )
	{
		this.listeTables = listeTables;
	}

}