package fr.univsmb.etu;

/**
 *
 */
public class EnPreparation extends EtatTicket
{

	/**
	 * Default constructor
	 */
	public EnPreparation ( Ticket tick ) {
		super( tick );
	}
	
	@Override
	public void envoie ()
	{
		// TODO Auto-generated method stub

	}
	
	@Override
	public String getNom ()
	{
		return "En Pr�paration";
	}
	
	@Override
	public void prepare ()
	{
		// TODO Auto-generated method stub

	}
	
	@Override
	public void suivant ()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void termine ()
	{
		Prepare etat = new Prepare( this.ticket );
		this.ticket.changeEtat( etat );
	}
}