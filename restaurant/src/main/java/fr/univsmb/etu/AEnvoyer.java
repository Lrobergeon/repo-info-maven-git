package fr.univsmb.etu;

/**
 *
 */
public class AEnvoyer extends EtatTicket
{

	/**
	 * Default constructor
	 */
	public AEnvoyer ( Ticket tick ) {
		super( tick );
	}
	
	@Override
	public void envoie ()
	{
		EstEnvoye etat = new EstEnvoye( this.ticket );
		// On cherche si le ticket qu'on envoie est le premier ticket de la commande
		if ( this.ticket.commande.listeTickets.get( 0 ).equals( this.ticket ) )
		{
			// Si c'est le premier, alors la commande d�marre (elle change d'etat)
			this.ticket.commande.debut();
		}
		this.ticket.changeEtat( etat );
	}
	
	@Override
	public String getNom ()
	{
		return "A Envoyer";
	}
	
	@Override
	public void prepare ()
	{
		// TODO Auto-generated method stub

	}
	
	@Override
	public void suivant ()
	{
		// TODO Auto-generated method stub

	}
	
	@Override
	public void termine ()
	{
		// TODO Auto-generated method stub

	}
	
}