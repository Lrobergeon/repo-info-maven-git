package fr.univsmb.etu;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class Carte
{

	/**
	 *
	 */
	protected ListeMenu						listeMenu;

	/**
	 *
	 */
	protected Map<CategoriePlat, ListePlat>	listePlats;

	/**
	 * Default constructor
	 */
	public Carte () {
		this.listePlats = new HashMap<CategoriePlat, ListePlat>();
		this.listeMenu = new ListeMenu();

		// Definition des listes de plats
		ListePlat listeEntree = new ListePlat();
		ListePlat listePoisson = new ListePlat();
		ListePlat listeViande = new ListePlat();
		ListePlat listeDessert = new ListePlat();
		ListePlat listeBoisson = new ListePlat();
		
		// Definition des listes de menus
		Menu menuJ = new Menu( "Menu du jour", 13.5 );
		Menu menuClassique = new Menu( "Menu classique", 15 );

		// Liste des entr�es
		Entree salade = new Entree( "Salade", 5.0 );
		Entree oeufs = new Entree( "Oeufs", 5.0 );
		listeEntree.plats.add( salade );
		listeEntree.plats.add( oeufs );

		// Liste des poissons
		Poisson cabillaud = new Poisson( "Cabillaud", 8.50 );
		Poisson saumon = new Poisson( "Saumon", 9.0 );
		listePoisson.plats.add( cabillaud );
		listePoisson.plats.add( saumon );

		// Liste des viandes
		Viande boeuf = new Viande( "Boeuf", 10.50 );
		Viande dinde = new Viande( "Dinde", 9.50 );
		listeViande.plats.add( boeuf );
		listeViande.plats.add( dinde );

		// Liste des desserts
		Dessert gateauChocolat = new Dessert( "Gateau au chocolat", 3.50 );
		Dessert glaces = new Dessert( "Glaces", 3.0 );
		listeDessert.plats.add( gateauChocolat );
		listeDessert.plats.add( glaces );

		// Liste des boissons
		Boisson coca = new Boisson( "Coca-cola", 2.50 );
		Boisson biere = new Boisson( "Biere", 2.40 );
		listeBoisson.plats.add( coca );
		listeBoisson.plats.add( biere );
		
		// Liste du menu du jour
		menuJ.plats.add( salade );
		menuJ.plats.add( dinde );
		menuJ.plats.add( glaces );
		
		// Liste du menu classique
		menuClassique.plats.add( oeufs );
		menuClassique.plats.add( saumon );
		menuClassique.plats.add( gateauChocolat );
		
		// Ajout des listes de plats avec leur cat�gorie et des menus
		this.listePlats.put( CategoriePlat.ENTREE, listeEntree );
		this.listePlats.put( CategoriePlat.POISSON, listePoisson );
		this.listePlats.put( CategoriePlat.VIANDE, listeViande );
		this.listePlats.put( CategoriePlat.DESSERT, listeDessert );
		this.listePlats.put( CategoriePlat.BOISSON, listeBoisson );
		
		this.listeMenu.menus.add( menuJ );
		this.listeMenu.menus.add( menuClassique );
	}

	/**
	 * @param cat
	 */
	public Categorie recupereCategorie ( CategoriePlat cat )
	{
		if ( cat == CategoriePlat.MENU )
		{
			return this.listeMenu;
		} else
		{
			return this.listePlats.get( cat );
			
		}

	}

}