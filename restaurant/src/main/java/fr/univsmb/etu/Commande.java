package fr.univsmb.etu;

import java.util.ArrayList;

/**
 *
 */
public class Commande
{

	/**
	 *
	 */
	protected ListePlat			listePlat;

	/**
	 *
	 */
	protected ListeMenu			listeMenu;

	/**
	 *
	 */
	protected ArrayList<Ticket>	listeTickets;

	/**
	 *
	 */
	protected EtatCommande		etat;
	
	/**
	 *
	 */
	protected Table				table;
	
	/**
	 * Default constructor
	 */
	public Commande () {
		this.listeTickets = new ArrayList<Ticket>();
		this.etat = EtatCommande.ATTENTE;
	}

	/**
	 * @param prod
	 */
	public void ajoutCommande ( Produit prod )
	{
		this.etat = EtatCommande.COMMANDE;
		this.listeMenu = new ListeMenu();
		this.listePlat = new ListePlat();
	}

	/**
	 * @param listeTicket
	 */
	public void ajoutTicket ( ListePlat listeTicket )
	{
		Ticket t1 = new Ticket();
		t1.commande = this;
		t1.ajoutListe( listeTicket );
		this.listeTickets.add( t1 );
	}

	/**
	 *
	 */
	public ListePlat creeTicket ()
	{
		ListePlat listeTotal = new ListePlat();

		for ( Produit p : this.listePlat.plats )
		{
			listeTotal.plats.add( p );
		}

		for ( Produit p : this.listeMenu.menus )
		{
			Menu m = (Menu) p;
			m.recupereListePlat();

			for ( Produit p2 : m.plats )
			{
				listeTotal.plats.add( p2 );
			}

		}

		return listeTotal;

	}
	
	/**
	 * @param tick
	 */
	public void debut ()
	{
		this.etat = EtatCommande.EN_COUR;
	}
	
	/**
	 * @param tick
	 */
	public void fin ()
	{
		this.etat = EtatCommande.TERMINE;
	}
}