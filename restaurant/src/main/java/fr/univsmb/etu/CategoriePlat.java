package fr.univsmb.etu;

/**
 *
 */
public enum CategoriePlat
{
	MENU, ENTREE, POISSON, VIANDE, DESSERT, BOISSON
}