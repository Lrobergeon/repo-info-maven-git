package fr.univsmb.etu;

import java.util.ArrayList;

/**
 *
 */
public class Menu extends Produit
{

	/**
	 *
	 */
	protected ArrayList<Plat>	plats;

	/**
	 * Default constructor
	 */
	public Menu ( String n, double p ) {
		super( n, p );
		this.plats = new ArrayList<Plat>();
		
	}

	//////////////////////////////

	/**
	 * @return
	 */
	public ListePlat recupereListePlat ()
	{
		ListePlat lp = new ListePlat();
		for ( Produit p2 : this.plats )
		{
			lp.plats.add( p2 );
		}
		return lp;
	}

}