package fr.univsmb.etu;

/**
 *
 */
public class Prepare extends EtatTicket
{

	/**
	 * Default constructor
	 */
	public Prepare ( Ticket tick ) {
		super( tick );
	}
	
	@Override
	public void envoie ()
	{
		// TODO Auto-generated method stub

	}
	
	@Override
	public String getNom ()
	{
		return "Pr�par�";
	}
	
	@Override
	public void prepare ()
	{
		// TODO Auto-generated method stub

	}
	
	@Override
	public void suivant ()
	{
		Servi etat = new Servi( this.ticket );
		// On cherche si le ticket qu'on sert est le dernier ticket de la commande
		if ( this.ticket.commande.listeTickets.get( this.ticket.commande.listeTickets.size() - 1 ).equals( this.ticket ) )
		{
			// Si c'est le dernier, alors la commande est termin�e (elle change d'etat)
			this.ticket.commande.fin();
		}
		this.ticket.changeEtat( etat );
	}

	@Override
	public void termine ()
	{
		// TODO Auto-generated method stub

	}

}