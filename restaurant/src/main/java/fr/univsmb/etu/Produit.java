package fr.univsmb.etu;

/**
 *
 */
public abstract class Produit
{

	/**
	 *
	 */
	protected String	nom;

	/**
	 *
	 */
	protected double	prix;

	/**
	 * Default constructor
	 */
	public Produit ( String n, double p ) {
		this.nom = n;
		this.prix = p;
	}

	/**
	 * @return
	 */
	public String recupereNom ()
	{
		return this.nom;
	}

	/**
	 * @return
	 */
	public double recuperePrix ()
	{
		return this.prix;
	}

}