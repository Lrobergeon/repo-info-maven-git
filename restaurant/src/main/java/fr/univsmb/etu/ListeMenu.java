package fr.univsmb.etu;

import java.util.ArrayList;

/**
 *
 */
public class ListeMenu extends Categorie
{
	
	/**
	 *
	 */
	protected ArrayList<Produit>	menus;
	
	/**
	 * Default constructor
	 */
	public ListeMenu () {
		this.menus = new ArrayList<Produit>();
	}

	/**
	 * @param prod
	 */
	@Override
	public void ajoutListe ( Produit prod )
	{
		Menu m = (Menu) prod;
		this.menus.add( m );
	}

	///////////////////////////////////

	/**
	 * @return
	 */
	@Override
	public ArrayList<Produit> recupereListe ()
	{

		return this.menus;

	}

}