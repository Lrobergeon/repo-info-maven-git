package fr.univsmb.etu;

import java.util.ArrayList;

/**
 *
 */
public class Ticket
{
	/**
	 *
	 */
	protected Commande				commande;
	
	/**
	 *
	 */
	protected ArrayList<Produit>	listePlats;

	/**
	 *
	 */
	protected EtatTicket			etat;

	/**
	 * Default constructor
	 */
	public Ticket () {
		this.listePlats = new ArrayList<Produit>();
		this.etat = new AEnvoyer( this );
	}

	/**
	 * @param listeTicket
	 */
	public void ajoutListe ( ListePlat listeTicket )
	{
		for ( Produit p : listeTicket.plats )
		{
			this.listePlats.add( p );
		}
	}

	/*****
	 * Changement des �tats du tickets
	 *****/

	/**
	 * @param tick
	 */
	public void changeEtat ( EtatTicket e )
	{
		this.etat = e;
	}

	/**
	 * @param tick
	 */
	public void envoie ()
	{
		this.etat.envoie();
	}

	/**
	 * @param tick
	 */
	public void prepare ()
	{
		this.etat.prepare();
	}

	/**
	 *
	 */
	public void suivant ()
	{
		this.etat.suivant();
	}

	/**
	 *
	 */
	public void termine ()
	{
		this.etat.termine();
	}

}