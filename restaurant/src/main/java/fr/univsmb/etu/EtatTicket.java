package fr.univsmb.etu;

/**
 *
 */
public abstract class EtatTicket
{

	protected Ticket	ticket;

	/**
	 * Default constructor
	 */
	public EtatTicket ( Ticket tick ) {
		this.ticket = tick;
	}

	/**
	 * @param tick
	 */
	public abstract void envoie ();

	/**
	 *
	 */
	public abstract String getNom ();

	/**
	 * @param tick
	 */
	public abstract void prepare ();

	/**
	 *
	 */
	public abstract void suivant ();

	/**
	 *
	 */
	public abstract void termine ();

}