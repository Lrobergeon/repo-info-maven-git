package fr.univsmb.etu;

import java.util.ArrayList;

/**
 *
 */
public abstract class Categorie
{
	
	/**
	 * Default constructor
	 */
	public Categorie () {}

	/**
	 * @param prod
	 */
	public abstract void ajoutListe ( Produit prod );

	////////////////////////////////

	/**
	 * @return
	 */
	public abstract ArrayList<Produit> recupereListe ();

}