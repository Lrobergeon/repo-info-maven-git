package fr.univsmb.etu;

import java.util.ArrayList;

/**
 *
 */
public class ListePlat extends Categorie
{

	/**
	 *
	 */
	protected ArrayList<Produit>	plats;

	/**
	 * Default constructor
	 */
	public ListePlat () {
		this.plats = new ArrayList<Produit>();
	}

	/**
	 * @param prod
	 */
	@Override
	public void ajoutListe ( Produit prod )
	{
		this.plats.add( prod );
	}

	////////////////////////////

	/**
	 * @return
	 *
	 */
	@Override
	public ArrayList<Produit> recupereListe ()
	{
		return this.plats;
	}
	
}