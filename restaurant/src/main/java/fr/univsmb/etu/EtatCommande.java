package fr.univsmb.etu;

/**
 *
 */
public enum EtatCommande
{
	ATTENTE, COMMANDE, EN_COUR, TERMINE
}