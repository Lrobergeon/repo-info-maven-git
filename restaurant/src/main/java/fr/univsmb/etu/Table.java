package fr.univsmb.etu;

/**
 *
 */
public class Table
{

	/**
	 *
	 */
	protected int		numero;

	/**
	 *
	 */
	protected int		personne;

	/**
	 *
	 */
	protected Commande	commande;

	/**
	 * Default constructor
	 */
	public Table ( int n ) {
		this.numero = n;
		this.personne = 0;
	}

	/**
	 *
	 */
	public void debutCommande ()
	{
		this.commande.listePlat = new ListePlat();
		this.commande.listeMenu = new ListeMenu();
	}

	/**
	 * @param listeTicket
	 */
	public void initialiseTicket ( ListePlat listeTicket )
	{
		this.commande.ajoutTicket( listeTicket );
	}

	/**
	 * @param pers
	 */
	public void installeTable ( int pers )
	{
		this.personne = pers;
		this.commande = new Commande();
		this.commande.table = this;
	}

}