package fr.univsmb.etu;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 */
public class Restaurant
{

	private static Scanner	sc;
	
	// Affiche la r�capitulatif de la commande d'une table, et le total a payer
	public static void addition ( Restaurant grillon )
	{
		double addition = 0;
		int choix = 0;
		Table tableCourrant;

		System.out.println( "Quelle table veut payer ? (0 pour annuler)" );
		Restaurant.afficheTableEtat( grillon, EtatCommande.TERMINE );
		choix = Restaurant.recupereChoix();
		if ( choix != 0 )
		{
			int index;
			tableCourrant = grillon.listeTables.get( choix - 1 );
			addition = grillon.payer( tableCourrant );
			index = grillon.confirmePayement( addition );
			if ( index == 1 )
			{
				tableCourrant.commande = null;
				tableCourrant.personne = 0;
			}
		}

	}
	
	// Menu principale, affiche les options disponibles
	public static void afficheAction ()
	{
		System.out.println( "*****************" );
		System.out.println( "Que voulez-vous faire ? " );
		System.out.println( "1) Installer des clients" );
		System.out.println( "2) Consulter la carte" );
		System.out.println( "3) Effectuer une commander" );
		System.out.println( "4) Voir les tickets de chaque table" );
		System.out.println( "5) Changer l'etat d'un ticket" );
		System.out.println( "6) Payer" );

	}

	// Fonction qui affiche la carte
	public static void afficheCarte ( Restaurant grillon )
	{
		CategoriePlat cat;
		boolean fini = false;
		int choix;
		
		// Boucle qui affiche les produits du restaurant
		while ( !fini )
		{
			System.out.println( "*****************" );
			grillon.afficheCategorie();
			System.out.println( "Que voulez-vous consulter ? ( 0 pour finir)" );
			choix = Restaurant.recupereChoix();
			if ( choix > 0 )
			{ // Tant que la consultation n'est pas finie
				cat = CategoriePlat.values()[ choix - 1 ];
				grillon.consulteCategorie( cat );

			} else
			{
				fini = true;
			}
			System.out.println();
		}
		
	}
	
	// Affiche les commandes de chaque table
	public static void afficheCommande ( Table tableCourrant )
	{
		System.out.println( "*****************" );
		System.out.println( "*****************" );
		System.out.println( "Voici la liste de vos tickets" );

		int nb = 1;
		System.out.println( "Commande pour la table " + tableCourrant.numero + " ou sont installees " + tableCourrant.personne
				+ " personnes" );
		for ( Ticket t : tableCourrant.commande.listeTickets )
		{
			System.out.println( "Ticket numero " + nb );
			for ( Produit p : t.listePlats )
			{
				System.out.println( "	" + p.nom );
			}
			nb++ ;
		}
	}

	// Affiche une liste de plat en parametre
	public static void afficheListePlats ( ArrayList<Produit> listeP )
	{
		for ( Produit p : listeP )
		{
			System.out.print( p.nom + " " );
		}
	}
	
	public static void afficheListeProduit ( ArrayList<Produit> lp )
	{
		for ( Produit p : lp )
		{
			System.out.print( p.nom + " " );
		}
	}

	// Fonction qui affiche la liste des tables
	public static void afficheTable ( Restaurant grillon )
	{
		int index = 1;
		for ( Table t : grillon.listeTables )
		{
			System.out.println( index + ") Table" + t.numero );
			index++ ;

		}
	}
	
	// Fonction qui affiche la liste des tables dans un etat donn�e (ex : uniquement les table en attentes)
	public static void afficheTableEtat ( Restaurant r, EtatCommande etat )
	{
		int index = 1;
		for ( Table t : r.listeTables )
		{
			if ( t.commande != null )
			{
				if ( t.commande.etat == etat )
				{
					System.out.println( index + ") Table " + index );
				}
			}
			index++ ;
		}
	}
	
	// Affiche la liste des tickets pr�sent dans le restaurant
	public static void afficheTickets ( Restaurant grillon )
	{
		int indexTable = 1;
		int indexTicket = 1;
		for ( Table t : grillon.listeTables )
		{
			System.out.print( "Table " + indexTable + ") " );
			System.out.print( "Nombre de personne : " + t.personne );
			System.out.print( " -> Etat commande : " );
			if ( t.commande == null )
			{
				System.out.println( " Pas de commande" );
			} else
			{
				System.out.print( " " + t.commande.etat );
				System.out.println();
				System.out.print( "     ===> " );
				for ( Ticket ti : t.commande.listeTickets )
				{
					System.out.print( "  << Ticket " + indexTicket + ") [" + ti.etat.getNom() + "] " );
					Restaurant.afficheListePlats( ti.listePlats );
					System.out.print( ">>  " );
					indexTicket++ ;
				}

				System.out.println();
				indexTicket = 1;
			}
			indexTable++ ;
		}
		System.out.println();
	}
	
	// Affiche seulement la liste des tickets dans l'etat souhait�
	public static void afficheTicketsEtat ( EtatTicket etat, ArrayList<Ticket> listeTicket )
	{

		int index = 1;
		int indexTicket = 1;
		System.out.println( "Liste des tickets : " );
		
		for ( Ticket t : listeTicket )
		{

			if ( t.etat.getClass().equals( etat.getClass() ) )
			{
				System.out.print( index + ") Commande " + t.commande.table.numero + ") " );
				indexTicket = t.commande.listeTickets.indexOf( t ) + 1;
				System.out.print( "<< Ticket " + indexTicket + ") [" + t.etat.getNom() + "] " );
				Restaurant.afficheListeProduit( t.listePlats );
				System.out.println( ">>  " );
				System.out.println();
			}
			index++ ;
		}
	}
	
	// Change l'etat d'un ticket
	public static void changeEtatTicket ( Restaurant grillon )
	{
		int choix;
		ArrayList<Ticket> listeTicket = new ArrayList<Ticket>();

		System.out.println( "Quelle personne effectue une action ? (0 pour annuler)" );
		System.out.println( "1) Serveur" );
		System.out.println( "2) Cuisinier/Barman" );
		choix = Restaurant.recupereChoix();

		listeTicket = grillon.recupereTicket();

		if ( choix == 1 ) // Role du serveur
		{
			System.out.println( "Que voulez-vous faire (Serveur) ? (0 pour annuler)" );
			System.out.println( "1) Envoyer un ticket en cuisine/bar" );
			System.out.println( "2) Recuperer un ticket" );
			choix = Restaurant.recupereChoix();
			System.out.println();

			if ( choix == 1 )
			{
				Restaurant.afficheTicketsEtat( new AEnvoyer( null ), listeTicket );
				System.out.println( "Quel ticket voulez-vous envoyer ? (0 pour annuler)" );
				choix = Restaurant.recupereChoix();
				if ( choix != 0 )
				{
					listeTicket.get( choix - 1 ).envoie();
				}
			} else if ( choix == 2 )
			{
				Restaurant.afficheTicketsEtat( new Prepare( null ), listeTicket );
				System.out.println( "Quel ticket voulez-vous recuperer ? (0 pour annuler)" );
				choix = Restaurant.recupereChoix();
				if ( choix != 0 )
				{
					listeTicket.get( choix - 1 ).suivant();
				}
			}

		} else if ( choix == 2 ) // Role du cuisinier ou barman
		{
			System.out.println( "Que voulez-vous faire (cuisinier/barman) ? (0 pour annuler)" );
			System.out.println( "1) Pr�parer un ticket" );
			System.out.println( "2) Terminer un ticket" );
			choix = Restaurant.recupereChoix();
			System.out.println();

			if ( choix == 1 )
			{
				Restaurant.afficheTicketsEtat( new EstEnvoye( null ), listeTicket );
				System.out.println( "Quel ticket voulez-vous pr�parer ? (0 pour annuler)" );
				choix = Restaurant.recupereChoix();
				if ( choix != 0 )
				{
					listeTicket.get( choix - 1 ).prepare();;
				}
			} else if ( choix == 2 )
			{
				Restaurant.afficheTicketsEtat( new EnPreparation( null ), listeTicket );
				System.out.println( "Quel ticket voulez-vous terminer ? (0 pour annuler)" );
				choix = Restaurant.recupereChoix();
				if ( choix != 0 )
				{
					listeTicket.get( choix - 1 ).termine();
				}
			}
		}

	}
	
	// effectue une commande dans le restaurant
	public static void effectueCommande ( Restaurant grillon )
	{
		int choix;
		Table tableCourrant;
		ListePlat listeTotal = new ListePlat();

		// Initialisation de la table (Quelle table et nombre de personnes)
		System.out.println( "*****************" );
		System.out.println( "Quelle table veut commander ?" );
		Restaurant.afficheTableEtat( grillon, EtatCommande.ATTENTE );
		choix = Restaurant.recupereChoix();
		tableCourrant = grillon.listeTables.get( choix - 1 );

		tableCourrant.debutCommande();

		// Ajoute des plats dans la commande
		Restaurant.prepareCommande( grillon, tableCourrant );
		
		System.out.println();
		System.out.println( "*****************" );
		System.out.println( "Commande confirm�e, voici la liste :" );
		listeTotal = tableCourrant.commande.creeTicket();

		// Trie la commande en ticket
		Restaurant.trieCommande( tableCourrant, listeTotal );
		
		// Change l'etat de la table
		tableCourrant.commande.etat = EtatCommande.COMMANDE;
		
		// Affiche le recapitulatif de la table
		System.out.println( "COMMANDE VALIDE (r�capitulatif)" );
		Restaurant.afficheCommande( tableCourrant );
		System.out.println();

	}

	
	// Installe des clients � une table
	public static void installeClient ( Restaurant grillon )
	{
		int choix;
		Table tableCourrant;

		System.out.println( "*****************" );
		System.out.println( "A quelle table sont install� les clients ?" );
		Restaurant.afficheTable( grillon );
		choix = Restaurant.recupereChoix();
		tableCourrant = grillon.listeTables.get( choix - 1 );
		System.out.println( "Combien de personnes sont install�es ?" );
		choix = Restaurant.recupereChoix();
		tableCourrant.installeTable( choix );

	}
	
	/**
	 * Fonction qui lance le programme principal
	 */
	public static void main ( String args[] )
	{
		// Cr�ation du restaurant
		Restaurant grillon = new Restaurant( "Grillon" );
		
		// Cr�ation des tables
		Table table1 = new Table( 1 );
		Table table2 = new Table( 2 );
		grillon.listeTables.add( table1 );
		grillon.listeTables.add( table2 );
		
		// Cr�ation des serveurs avec attribution des tables, du barman et du cuisinier
		Serveur serge = new Serveur( "Serge" );
		serge.listeTables.add( table1 );
		serge.listeTables.add( table2 );
		
		// Initialisation
		int choix = 0;

		while ( true )
		{
			Restaurant.afficheAction();
			choix = Restaurant.recupereChoix();

			if ( choix == 1 )
			{
				Restaurant.installeClient( grillon );

			} else if ( choix == 2 ) // Consulte carte
			{
				Restaurant.afficheCarte( grillon );

			} else if ( choix == 3 ) // Effectue commande
			{
				Restaurant.effectueCommande( grillon );

			} else if ( choix == 4 ) // voir liste ticket
			{
				Restaurant.afficheTickets( grillon );

			} else if ( choix == 5 ) // gere ticket
			{
				Restaurant.changeEtatTicket( grillon );
				
			} else if ( choix == 6 ) // Payement
			{
				Restaurant.addition( grillon );
				
			}
		}
	}
	
	// Fonction qui ajoute des plats/menus dans la commande
	public static void prepareCommande ( Restaurant grillon, Table tableCourrant )
	{
		CategoriePlat cat;
		boolean fini = false;
		int choix;
		
		while ( !fini )
		{
			System.out.println( "*****************" );
			grillon.afficheCategorie();
			System.out.println();
			System.out.println( "Que voulez-vous commander ? ( 0 pour finir)" );
			choix = Restaurant.recupereChoix();
			
			if ( choix > 0 )
			{ // Tant que la commande n'est pas finie
				cat = CategoriePlat.values()[ choix - 1 ];
				grillon.consulteCategorie( cat );
				System.out.println( "Quel est le numero de votre commande ? (0 pour annuler)" );
				choix = Restaurant.recupereChoix();

				if ( choix > 0 )
				{
					if ( cat == CategoriePlat.MENU )
					{ // Ajout, dans la liste, des menus de la table
						tableCourrant.commande.listeMenu.ajoutListe( grillon.carte.listeMenu.menus.get( choix - 1 ) );
					} else
					{ // Ajout, dans la liste, des plats de la table
						tableCourrant.commande.listePlat.ajoutListe( grillon.carte.listePlats.get( cat ).plats.get( choix - 1 ) );
					}
				}

			} else
			{
				fini = true;
			}
		}
	}

	/******** Fonction qui simplifie le code du main ********/
	/********************************************************/
	
	// Fonction qui recupere le choix de l'utilisateur
	public static int recupereChoix ()
	{
		Restaurant.sc = new Scanner( System.in );
		int choix = Restaurant.sc.nextInt();
		return choix;
	}

	// Fonction qui trie la commande en ticket
	public static void trieCommande ( Table tableCourrant, ListePlat listeTotal )
	{
		int choix;
		ListePlat listeCourrant = new ListePlat();
		boolean ticketFini = false;
		int index = 1;
		int nbTicket = 1;
		Produit prodCourrant;
		
		while ( listeTotal.plats.size() > 0 )
		{

			// Boucle qui ajoute une liste de produits dans un ticket
			while ( !ticketFini && listeTotal.plats.size() > 0 )
			{
				for ( Produit p : listeTotal.plats )
				{// Affiche la liste des plats pour les ranger en ticket
					System.out.println( index + ") " + p.nom );
					index++ ;
				}

				System.out.println( "Que voulez mettre dans le ticket " + nbTicket + "? (0 pour valider ce ticket)" );
				choix = Restaurant.recupereChoix();
				if ( choix > 0 )
				{ // Ajoute un plat dans la liste courante
					prodCourrant = listeTotal.plats.get( choix - 1 );
					listeTotal.plats.remove( prodCourrant );
					listeCourrant.ajoutListe( prodCourrant );
				} else
				{
					ticketFini = true;
					nbTicket++ ;
				}
				index = 1;
			}

			// On cree le ticket avec la liste qu'on a choisi
			tableCourrant.initialiseTicket( listeCourrant );
			listeCourrant.plats.clear();
			ticketFini = false;

		}
	}

	/**
	 *
	 */
	protected String				nom;
	
	/**
	 *
	 */
	protected Carte					carte;
	
	/**
	 *
	 */
	protected ArrayList<Table>		listeTables;

	/**
	 *
	 */
	protected ArrayList<Serveur>	listeServeurs;

	/**
	 *
	 */
	protected ArrayList<Ticket>		cuisine;
	
	/**
	 * Default constructor
	 */
	public Restaurant ( String n ) {
		this.nom = n;
		this.carte = new Carte();
		this.listeTables = new ArrayList<Table>();
		this.listeServeurs = new ArrayList<Serveur>();
		this.cuisine = new ArrayList<Ticket>();
	}

	/**
	 * @param listeTickets
	 * @param listeTickets
	 */
	public double afficheAddition ( ListeMenu listeM, ListePlat listeP )
	{
		double prix = 0;
		System.out.println( "Voici le r�capitulatif de votre commande :" );

		for ( Produit m : listeM.menus )
		{
			System.out.println( "    " + m.nom + "  -  " + m.prix );
			prix += m.prix;
		}
		for ( Produit m : listeP.plats )
		{
			System.out.println( "    " + m.nom + "  -  " + m.prix );
			prix += m.prix;
		}
		System.out.println();
		System.out.println( "Cela fait un total de " + prix );
		return prix;
	}

	/**
	 * Affiche la liste des categories des differents plats
	 */
	public void afficheCategorie ()
	{
		System.out.println( "Liste des categories de plats disponibles :" );
		System.out.println( "1) Menu" );
		System.out.println( "2) Entree" );
		System.out.println( "3) Poisson" );
		System.out.println( "4) Viande" );
		System.out.println( "5) Dessert" );
		System.out.println( "6) Boisson" );
	}

	/**
	 * @param add
	 * @return
	 *
	 */
	public int confirmePayement ( double add )
	{
		int choix;
		System.out.println( "Confirmer le payement de " + add );
		System.out.println( "1) Oui" );
		System.out.println( "2) Non" );
		choix = Restaurant.recupereChoix();
		return choix;
	}
	
	/**
	 * @param cat
	 */
	public void consulteCategorie ( CategoriePlat cat )
	{
		int index = 1;
		System.out.println( "Voici la liste des " + cat );
		Categorie c = this.carte.recupereCategorie( cat );

		if ( cat == CategoriePlat.MENU )
		{
			for ( Produit p : c.recupereListe() )
			{
				Menu m = (Menu) p;
				System.out.println( index + ") " + m.nom + "    " + m.prix );
				for ( Produit p2 : m.plats )
				{
					System.out.println( "    " + p2.nom );
				}
				index++ ;
			}
		} else
		{
			for ( Produit p : c.recupereListe() )
			{
				System.out.println( index + ") " + p.nom + "    " + p.prix );
				index++ ;
			}
		}
	}
	
	/**
	 * @param tab
	 * @return
	 */
	public double payer ( Table tab )
	{
		return this.afficheAddition( tab.commande.listeMenu, tab.commande.listePlat );
	}

	/**
	 *
	 */
	public ArrayList<Ticket> recupereTicket ()
	{
		this.cuisine = new ArrayList<Ticket>();
		for ( Table tab : this.listeTables )
		{
			if ( tab.commande != null )
			{
				for ( Ticket tic : tab.commande.listeTickets )
				{
					this.cuisine.add( tic );
				}
			}
		}
		return this.cuisine;
	}
}